const express = require('express');

const db = require('./db/db');
const routes = require('./controller/routes/index');

const app = express();
const port = 3000;

app.use(express.json());
app.use((req, res, next) => {
  db.raw('select 1+1 as result').then(function () {
    next();
  }).catch(() => {
    throw new Error('No db connection');
  });
});

routes(app);

app.listen(port, () => {
  console.log(`App listening at http://localhost:${port}`);
});

// Do not change this line
module.exports = { app };