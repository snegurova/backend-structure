const schemaValidation = (schema, property) => {
  return (req, res, next) => {
    const { error } = schema.validate(req[property]);
    if (error) {
      const errorMessage = error.details[0].message;
      res.status(400).send({ error: errorMessage });
      return;
    }
    next();
  };
};

exports.schemaValidation = schemaValidation;