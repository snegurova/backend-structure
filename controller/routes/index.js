const healthRoutes = require('./healthRoutes');
const usersRoutes = require('./usersRoutes');
const transactionsRoutes = require('./transactionsRoutes');
const eventsRoutes = require('./eventsRoutes');
const betsRoutes = require('./betsRoutes');
const statsRoutes = require('./statsRoutes');

module.exports = (app) => {
  app.use('/health', healthRoutes);
  app.use('/users', usersRoutes);
  app.use('/transactions', transactionsRoutes);
  app.use('/events', eventsRoutes);
  app.use('/bets', betsRoutes);
  app.use('/stats', statsRoutes);
}