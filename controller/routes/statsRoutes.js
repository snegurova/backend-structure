const {Router} = require('express');
const db = require('../../db/db');
const jwt = require("jsonwebtoken");

const router = Router();

router
  .get("/", async (req, res) => {
    try {
      let token = req.headers['authorization'];
      if (!token) {
        return res.status(401).send({error: 'Not Authorized'});
      }
      token = token.replace('Bearer ', '');
      try {
        var tokenPayload = jwt.verify(token, process.env.JWT_SECRET);
        if (tokenPayload.type != 'admin') {
          throw new Error();
        }
      } catch (err) {
        return res.status(401).send({error: 'Not Authorized'});
      }
      
      const users = await db.select('id').from("user");
      const totalUsers = users.length;
      const bets = await db.select('id').from("bet");
      const totalBets = bets.length;
      const events = await db.select('id').from("event");
      const totalEvents = events.length;
      
      res.send({ totalUsers, totalBets, totalEvents});
    } catch (err) {
      console.log(err);
      res.status(500).send("Internal Server Error");
      return;
    }
  });

module.exports = router;