const Joi = require('joi');

const schemas = {
  userGET: Joi.object({
    id: Joi.string().uuid(),
  }).required(),
  userPOST: Joi.object({
    id: Joi.string().uuid(),
    type: Joi.string().required(),
    email: Joi.string().email().required(),
    phone: Joi.string().pattern(/^\+?3?8?(0\d{9})$/).required(),
    name: Joi.string().required(),
    city: Joi.string(),
  }).required(),
  userPUT: Joi.object({
    email: Joi.string().email(),
    phone: Joi.string().pattern(/^\+?3?8?(0\d{9})$/),
    name: Joi.string(),
    city: Joi.string(),
  }).required(),
  betPOST: Joi.object({
    id: Joi.string().uuid(),
    eventId: Joi.string().uuid().required(),
    betAmount: Joi.number().min(1).required(),
    prediction: Joi.string().valid('w1', 'w2', 'x').required(),
  }).required(),
  eventPOST: Joi.object({
    id: Joi.string().uuid(),
    type: Joi.string().required(),
    homeTeam: Joi.string().required(),
    awayTeam: Joi.string().required(),
    startAt: Joi.date().required(),
    odds: Joi.object({
      homeWin: Joi.number().min(1.01).required(),
      awayWin: Joi.number().min(1.01).required(),
      draw: Joi.number().min(1.01).required(),
    }).required(),
  }).required(),
  eventPUT: Joi.object({
    score: Joi.string().required(),
  }).required(),
  transactionPOST: Joi.object({
    id: Joi.string().uuid(),
    userId: Joi.string().uuid().required(),
    cardNumber: Joi.string().required(),
    amount: Joi.number().min(0).required(),
  }).required(),
};

module.exports = schemas;